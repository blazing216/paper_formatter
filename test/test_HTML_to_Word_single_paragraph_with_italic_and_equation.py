import unittest
import sys
sys.path.append('../src')
from HTML_input import HTMLReader
from Word_output import WordWriter


def test_HTML_to_Word_single_paragraph_with_italic_and_equation():
    reader = HTMLReader('<p>Inline Equation $E=mc^2$, '
            'where $E$ is the <em>energy</em>'
            ', $m$ the <em>mass</em> and $c$ the light speed. </p>')
    paper = reader.parse()
    writer = WordWriter(paper)

    writer.to_file('test_HTML_to_Word_single_paragraph_with_italic_and_equation.docx')
    print('test_HTML_to_Word_single_paragraph_with_italic_and_equation.docx')

if __name__ == '__main__':
    test_HTML_to_Word_single_paragraph_with_italic_and_equation()