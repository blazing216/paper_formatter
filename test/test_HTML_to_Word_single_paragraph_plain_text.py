import unittest
import sys
sys.path.append('../src')
from HTML_input import HTMLReader
from Word_output import WordWriter

def test_HTML_to_Word_single_paragraph_plain_text():
    reader = HTMLReader('<p>This is paragraph converted from HTML using Paper Formatter.</p>')
    paper = reader.parse()
    writer = WordWriter(paper)
    writer.to_file('test_HTML_to_Word_single_paragraph_plain_text.docx')
    print('test_HTML_to_Word_single_paragraph_plain_text.docx')

if __name__ == '__main__':
    test_HTML_to_Word_single_paragraph_plain_text()