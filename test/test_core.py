import unittest
import sys
sys.path.append('../src')
import core

class TestCore(unittest.TestCase):
    def test_default(self):
        paper = core.Paper()
        self.assertEqual(paper.title, None)
        self.assertEqual(paper.author, None)
        self.assertEqual(paper.affiliation, None)
        self.assertEqual(paper.abstract, None)
        self.assertEqual(paper.keywords, None)
        self.assertEqual(len(paper.body.sections), 0)
        self.assertEqual(paper.bibliography, None)
        self.assertEqual(paper.appendix, None)
        self.assertEqual(paper.supplimentary, None)
    
    def test_insert_unnamed_empty_section_nsection_eq_one(self):
        paper = core.Paper()
        paper.insert_section()
        self.assertEqual(paper.body.nsection, 1)

    def test_insert_named_empty_section_nsection_eq_one(self):
        paper = core.Paper()
        paper.insert_section('Test')
        self.assertEqual(paper.body.nsection, 1)
    
    def test_insert_named_empty_section_nsection_title_assigned(self):
        paper = core.Paper()
        paper.insert_section(title='Test')
        self.assertEqual(paper.body.section_names, ['Test'])

    def test_insert_two_named_empty_section_nsection_eq_two(self):
        paper = core.Paper()
        paper.insert_section(title='Test1')
        paper.insert_section(title='Test2')
        self.assertEqual(paper.body.nsection, 2)
    
    def test_insert_ten_named_empty_section_nsection_eq_ten(self):
        paper = core.Paper()
        for _ in range(10):
            paper.insert_section(title='Test')
        self.assertEqual(paper.body.nsection, 10)

    def test_insert_two_named_empty_section_nsection_title_assigned(self):
        paper = core.Paper()
        paper.insert_section(title='Test1')
        paper.insert_section(title='Test2')
        self.assertEqual(paper.body.section_names, ['Test1', 'Test2'])

    def test_insert_three_named_empty_section_nsection_title_assigned(self):
        paper = core.Paper()
        paper.insert_section(title='Test1')
        paper.insert_section(title='Test2')
        paper.insert_section(title='Test2')
        self.assertEqual(paper.body.section_names, ['Test1', 'Test2', 'Test2'])

    def test_insert_one_named_nonempty_section_nsection_body_assigned(self):
        paper = core.Paper()
        paragraph = core.Paragraph([core.InlineText('test text')])
        paper.insert_section(title='Test1', paragraphs=[paragraph])
        self.assertEqual(paper.body.sections[0].title, 'Test1')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'test text')

    def test_insert_two_named_nonempty_section_nsection_body_assigned(self):
        paper = core.Paper()
        paper.insert_section(title='Test1', 
            paragraphs=[core.Paragraph([core.InlineText('test text')])])
        paper.insert_section(title='Test2', 
            paragraphs=[core.Paragraph([core.InlineText('test text1')])])
        self.assertEqual(paper.body.sections[0].title, 'Test1')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'test text')
        self.assertEqual(paper.body.sections[1].title, 'Test2')
        self.assertEqual(paper.body.sections[1].paragraphs[0].elements[0].text, 'test text1')
        

    def test_insert_three_named_nonempty_section_nsection_body_assigned(self):
        paper = core.Paper()
        paper.insert_section(title='Test1', 
            paragraphs=[core.Paragraph([core.InlineText('test text')])])
        paper.insert_section(title='Test2', 
            paragraphs=[core.Paragraph([core.InlineText('test text1')])])
        paper.insert_section(title='Test3', 
            paragraphs=[core.Paragraph([core.InlineText('test text2')])])
        self.assertEqual(paper.body.sections[0].title, 'Test1')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'test text')
        self.assertEqual(paper.body.sections[1].title, 'Test2')
        self.assertEqual(paper.body.sections[1].paragraphs[0].elements[0].text, 'test text1')
        self.assertEqual(paper.body.sections[2].title, 'Test3')
        self.assertEqual(paper.body.sections[2].paragraphs[0].elements[0].text, 'test text2')

    def test_insert_nonempty_secion_with_InlineText(self):
        paper = core.Paper()
        paper.insert_section(title='Test1',
            paragraphs=[core.Paragraph([core.InlineText('test'),
                core.InlineText('text', italic=True)])])
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'test')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].italic, True)

    def test_insert_paragraph_to_Paper(self):
        paper = core.Paper()
        paper.insert_paragraph([core.InlineText('test'),
            core.InlineText('text', italic=True)])
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'test')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].italic, True)

    def test_insert_paragraph_to_Body(self):
        paper = core.Paper()
        paper.body.insert_paragraph([core.InlineText('test'),
            core.InlineText('text', italic=True)])
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'test')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].italic, True)

    def test_insert_paragraph_to_Section(self):
        paper = core.Paper()
        paper.insert_section()
        paper.body.sections[0].insert_paragraph([core.InlineText('test'),
            core.InlineText('text', italic=True)])
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'test')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].italic, True)

    def test_insert_empty_paragraph(self):
        paper = core.Paper()
        paper.insert_paragraph()
        self.assertEqual(len(paper.body.sections[0].paragraphs), 1)
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 0)

    def test_insert_figure_to_paragraph(self):
        paper = core.Paper()
        paper.insert_paragraph()
        self.assertEqual(len(paper.body.sections[0].paragraphs), 1)
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 0)

        paper.body.sections[0].paragraphs[0].insert_figure(core.DisplayFigure("image.png",
            caption="Figure 1. test image"))
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'figure')
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 1)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].figname,
            'image.png')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].caption,
            'Figure 1. test image')

    def test_insert_figure_to_section(self):
        paper = core.Paper()
        paper.insert_section()
        self.assertEqual(len(paper.body.sections), 1)
        self.assertEqual(len(paper.body.sections[0].paragraphs), 0)

        paper.body.sections[0].insert_figure(core.DisplayFigure("image.png",
            caption="Figure 1. test image"))
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'figure')
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 1)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].figname,
            'image.png')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].caption,
            'Figure 1. test image')

    def test_insert_figure_to_body(self):
        paper = core.Paper()
        self.assertEqual(len(paper.body.sections), 0)

        paper.body.insert_figure(core.DisplayFigure("image.png",
            caption="Figure 1. test image"))
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'figure')
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 1)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].figname,
            'image.png')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].caption,
            'Figure 1. test image')

    def test_insert_figure_to_paper(self):
        paper = core.Paper()
        self.assertEqual(len(paper.body.sections), 0)
        paper.insert_figure(core.DisplayFigure("image.png",
            caption="Figure 1. test image"))

        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'figure')
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 1)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].figname,
            'image.png')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].caption,
            'Figure 1. test image')




if __name__ == '__main__':
    unittest.main()