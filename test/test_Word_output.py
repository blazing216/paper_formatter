import unittest
import sys
from docx import Document

sys.path.append('../src')
from Word_output import WordWriter
from core import Paper


class TestWordWriter(unittest.TestCase):
    def test_default_input_paper_is_None(self):
        w = WordWriter()
        self.assertEqual(w.paper, None)

    def test_input_assigned(self):
        paper = Paper()
        paper.insert_section('Test', 'test text')
        w = WordWriter(paper)
        self.assertEqual(w.paper, paper)

        

if __name__ == '__main__':
    unittest.main()
