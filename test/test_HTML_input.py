import unittest
import sys
# sys.path.append('../src')
from HTML_input import HTMLReader, parse_figure


class TestHTMLInput(unittest.TestCase):
    def test_default_input_is_empty(self):
        reader = HTMLReader()
        self.assertEqual(reader.html, None)

    def test_input_assigned(self):
        reader = HTMLReader('test')
        self.assertEqual(reader.html, 'test')

    def test_parse_plain_text_paragraph(self):
        reader = HTMLReader('<p>html paragraph</p>')
        self.assertEqual(reader.html, '<p>html paragraph</p>')
        paper = reader.parse()
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'html paragraph')

    def test_parse_plain_text_italic_paragraph(self):
        reader = HTMLReader('<p>html paragraph <em>italic text</em></p>')
        paper = reader.parse()
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 2)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'html paragraph ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'italic text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].italic, True)

    def test_parse_two_plain_texts_italic_paragraph(self):
        reader = HTMLReader('<p>html paragraph <em>italic text</em> after italic</p>')
        paper = reader.parse()
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 3)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'html paragraph ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'italic text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].italic, True)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].text, ' after italic')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].italic, False)

    def test_parse_two_plain_texts_two_italic_paragraph(self):
        reader = HTMLReader('<p>html paragraph <em>italic text</em> after italic <em>italic text2</em></p>')
        paper = reader.parse()
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 4)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'html paragraph ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'italic text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].italic, True)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].text, ' after italic ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].text, 'italic text2')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].italic, True)

    def test_parse_two_italic_two_plain_texts_paragraph(self):
        reader = HTMLReader('<p><em>italic text</em> after italic <em>italic text2</em> html paragraph</p>')
        paper = reader.parse()
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 4)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'italic text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].italic, True)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, ' after italic ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].text, 'italic text2')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].italic, True)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].text, ' html paragraph')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].italic, False)

    def test_parse_two_italic_one_plain_texts_paragraph(self):
        reader = HTMLReader('<p><em>italic text</em> after italic <em>italic text2</em></p>')
        paper = reader.parse()
        self.assertEqual(len(paper.body.sections[0].paragraphs[0].elements), 3)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'italic text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].italic, True)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, ' after italic ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].italic, False)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].text, 'italic text2')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].italic, True)

    def test_parse_equation(self):
        reader = HTMLReader('<p>$E=mc^2$</p>')
        paper = reader.parse()
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'E=mc^2')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'formula')

    def test_parse_plain_text_equation_plain_text(self):
        reader = HTMLReader('<p>Inline Equation $E=mc^2$. Plain text</p>')
        paper = reader.parse()
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'Inline Equation ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'E=mc^2')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].type, 'formula')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].text, '. Plain text')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].type, 'normal')

    def test_parse_plain_text_equation_plain_text_italic(self):
        reader = HTMLReader('<p>Inline Equation $E=mc^2$, '
            'where $E$ is the <em>energy</em> '
            ',$m$ the <em>mass</em> and $c$ the light speed. </p>')
        paper = reader.parse()
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'Inline Equation ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'E=mc^2')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].type, 'formula')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].text, ', where ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[2].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].text, 'E')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].type, 'formula')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[4].text, ' is the ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[4].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[5].text, 'energy')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[5].type, 'italic')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[6].text, ' ,')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[6].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[7].text, 'm')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[7].type, 'formula')

    def test_break_htmls_to_two_paragraph_strings(self):
        reader = HTMLReader('''<p>Paragraph 1</p>
<p>Paragraph 2</p>''')
        paragraph_strings = reader.get_paragraphs()
        self.assertEqual(paragraph_strings[0], 'Paragraph 1')
        self.assertEqual(paragraph_strings[1], 'Paragraph 2')
        
    def test_two_plain_paragraphs(self):
        reader = HTMLReader('''<p>Paragraph 1</p>
<p>Paragraph 2</p>''')
        paper = reader.parse()
        self.assertEqual(paper.body.sections[0].title, None)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'Paragraph 1')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[0].text, 'Paragraph 2')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[0].type, 'normal')

    def test_two_paragraphs_with_italic_and_equation(self):
        reader = HTMLReader('''<p>Paragraph 1 <em>Inline equation</em> $E=mc^2$</p>
<p>Paragraph 2 <em>Inline equation 2</em> $E=mc^3$</p>''')
        paper = reader.parse()
        self.assertEqual(paper.body.sections[0].title, None)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'Paragraph 1 ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'Inline equation')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].type, 'italic')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].text, 'E=mc^2')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].type, 'formula')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[0].text, 'Paragraph 2 ')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[0].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[1].text, 'Inline equation 2')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[1].type, 'italic')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[3].text, 'E=mc^3')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[3].type, 'formula')

    def test_two_paragraphs_with_italic_and_equation_with_extra_spaces_ignored(self):
        reader = HTMLReader(''' <p>Paragraph 1 <em>Inline equation</em> $E=mc^2$</p> 

<p>Paragraph 2 <em>Inline equation 2</em> $E=mc^3$</p> ''')
        paper = reader.parse()
        self.assertEqual(paper.body.sections[0].title, None)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'Paragraph 1 ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'Inline equation')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].type, 'italic')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].text, 'E=mc^2')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].type, 'formula')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[0].text, 'Paragraph 2 ')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[0].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[1].text, 'Inline equation 2')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[1].type, 'italic')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[3].text, 'E=mc^3')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[3].type, 'formula')

    def test_two_paragraphs_with_italic_and_equation_with_extra_spaces_and_characters_ignored(self):
        reader = HTMLReader(''' ss df <p>Paragraph 1 <em>Inline equation</em> $E=mc^2$</p> 
        dffds
<p>Paragraph 2 <em>Inline equation 2</em> $E=mc^3$</p> dfdfd ''')
        paper = reader.parse()
        self.assertEqual(paper.body.sections[0].title, None)
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].text, 'Paragraph 1 ')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].text, 'Inline equation')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[1].type, 'italic')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].text, 'E=mc^2')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[3].type, 'formula')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[0].text, 'Paragraph 2 ')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[0].type, 'normal')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[1].text, 'Inline equation 2')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[1].type, 'italic')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[3].text, 'E=mc^3')
        self.assertEqual(paper.body.sections[0].paragraphs[1].elements[3].type, 'formula')

    def test_parse_across_multiline(self):
        reader = HTMLReader('''<p>test figure
</p>''')
        paras = reader.get_paragraphs()
        # print(paras)
        self.assertEqual(len(paras), 1)
        self.assertEqual(paras[0], 'test figure')

    def test_parse_across_multiline_complicate_example(self):
        reader = HTMLReader(''' ss df <p>
Paragraph 1 
<em>
Inline equation
</em> 
$E=mc^2$
</p> 
dffds
<p>
Paragraph 2 
<em>
Inline equation 2
</em> 
$E=mc^3$
</p> dfdfd ''')
        paras = reader.get_paragraphs()
        self.assertEqual(len(paras), 2)
        self.assertEqual(paras[0], 'Paragraph 1 <em>Inline equation</em> $E=mc^2$')
        self.assertEqual(paras[1], 'Paragraph 2 <em>Inline equation 2</em> $E=mc^3$')
        elements = reader.parse_paragraph(paras[0])
        self.assertEqual(elements[0].text, 'Paragraph 1 ')
        self.assertEqual(elements[0].type, 'normal')
        self.assertEqual(elements[1].text, 'Inline equation')
        self.assertEqual(elements[1].type, 'italic')
        self.assertEqual(elements[2].text, ' ')
        self.assertEqual(elements[2].type, 'normal')
        self.assertEqual(elements[3].text, 'E=mc^2')
        self.assertEqual(elements[3].type, 'formula')

    def test_parse_figure(self):
        reader = HTMLReader('''<p>
<figure>
<img src="image.png">
<figcaption>Figure 1. test figuare</figcaption>
</figure>
</p>''')

        paras = reader.get_paragraphs()
        self.assertEqual(len(paras), 1)
        self.assertEqual(paras[0], '<figure><img src="image.png"><figcaption>Figure 1. test figuare</figcaption></figure>')

        elements = parse_figure(paras[0])
        self.assertEqual(len(elements), 1)
        self.assertEqual(elements[0].type, 'figure')
        self.assertEqual(elements[0].figname, 'image.png')
        self.assertEqual(elements[0].caption, 'Figure 1. test figuare')

        paper = reader.parse()
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].type, 'figure')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].figname, 'image.png')
        self.assertEqual(paper.body.sections[0].paragraphs[0].elements[0].caption, 'Figure 1. test figuare')


if __name__ == '__main__':
    unittest.main()