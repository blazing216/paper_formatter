import unittest
import sys
sys.path.append('../src')
from HTML_input import HTMLReader
from Word_output import WordWriter

def test_HTML_to_Word_single_paragraph_text_with_italic():
    reader = HTMLReader('<p><em>italic text</em> normal text <em>italic text2</em> normal text</p>')
    paper = reader.parse()
    writer = WordWriter(paper)

    writer.to_file('test_HTML_to_Word_single_paragraph_text_with_italic.docx')
    print('test_HTML_to_Word_single_paragraph_text_with_italic.docx')

if __name__ == '__main__':
    test_HTML_to_Word_single_paragraph_text_with_italic()