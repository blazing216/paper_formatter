import unittest
import sys
sys.path.append('../src')
from HTML_input import HTMLReader
from Word_output import WordWriter


def test_HTML_to_Word_figure():
    html = ('''jkdjfldjf 
<p>
Paragraph 1: Inline Equation $E=mc^2$, 
where $E$ is the <em>energy</em>
, $m$ the <em>mass</em> and $c$ the light speed. 
</p> 
kjdkjfkjdkfjkdjfksjdf
<p>
$\\frac{\\mathrm{d}}{\\mathrm{d}x}\\cos(x) = -\\sin(x)$
</p>
<p>
Two consecutive figures in a sparagraph
</p>
<p>
<figure><img src="image.png"><figcaption>Figure 1. caption</figcaption></figure>
<figure><img src="image.png"><figcaption>Figure 2. caption</figcaption></figure>
</p>
<p>
Two figures with text in a paragraph
<figure><img src="image.png"><figcaption>Figure 3. caption</figcaption></figure>
Between two figures
<figure><img src="image.png"><figcaption>Figure 4. caption</figcaption></figure>
After the two figures
</p>
<p>
Paragraph 2: Inline Equation $E=mc^3$, 
where $E$ is the <em>energy</em>
, $m$ the <em>mass</em> and $c$ the light speed. 
</p> kjdkfjaldfj''')

    reader = HTMLReader(html)
    paper = reader.parse()
    writer = WordWriter(paper)

    writer.to_file('test_HTML_to_Word_figure.docx')
    print('test_HTML_to_Word_figure.docx')

if __name__ == '__main__':
    test_HTML_to_Word_figure()