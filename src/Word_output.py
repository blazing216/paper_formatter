from os import device_encoding
from docx import Document
from docx.shared import Inches
from Latex2Word import Latex2Word

l2w = Latex2Word()
class WordWriter:
    def __init__(self, paper=None):
        self.paper = paper

    def to_file(self, filename):
        document = Document()
        
        for section in self.paper.body.sections:
            for paragraph in section.paragraphs:
                p = None
                for element in paragraph.elements:
                    if element.type != 'figure' and p is None:
                        p = document.add_paragraph()
                    if element.type == 'italic':
                        p.add_run(element.text).font.italic = True
                    elif element.type == 'formula':
                        l2w.insert_to_paragraph(element.text, p)
                    elif element.type == 'figure':
                        document.add_picture(element.figname, width=Inches(2.5))
                        document.add_paragraph(element.caption)
                        p = None
                    else:
                        p.add_run(element.text)

        document.save(filename)