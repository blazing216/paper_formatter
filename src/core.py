class Paper:
    def __init__(self):
        self.title = None
        self.author = None
        self.affiliation = None
        self.abstract = None
        self.keywords = None
        self.body = PaperBody()
        self.bibliography = None
        self.appendix = None
        self.supplimentary = None

    def insert_section(self, title=None,
            paragraphs=None):
        # if self.body == None:
        #     self.body = PaperBody()
        self.body.insert_section(title=title,
            paragraphs=paragraphs)

    def insert_paragraph(self, elements=None):
        # if self.body.nsection == 0:
        #     self.body.insert_section()
        # self.body.sections[-1].insert_paragraph(elements)
        self.body.insert_paragraph(elements)

    def insert_figure(self, fig):
        self.body.insert_figure(fig)


class PaperBody:
    def __init__(self):
        self.sections = []

    def insert_section(self, title=None, paragraphs=None):
        self.sections.append(Section(title=title, paragraphs=paragraphs))

    def insert_paragraph(self, elements):
        if self.nsection == 0:
            self.sections.append(Section())
        self.sections[-1].insert_paragraph(elements)

    def insert_figure(self, fig):
        if self.nsection == 0:
            self.sections.append(Section())
        self.sections[-1].insert_figure(fig)

    @property
    def section_names(self):
        return [s.title for s in self.sections]

    @property
    def nsection(self):
        return len(self.sections)

class Section:
    def __init__(self, title=None, paragraphs=None):
        self.title = title
        if paragraphs is None:
            self.paragraphs = []
        else:
            self.paragraphs = paragraphs

    def insert_paragraph(self, elements=None):
        self.paragraphs.append(Paragraph(elements))

    def insert_figure(self, fig):
        if len(self.paragraphs) == 0:
            self.insert_paragraph()
        self.paragraphs[-1].insert_figure(fig)

class Paragraph:
    def __init__(self, elements=None):
        if elements is None:
            self.elements = []
        else:
            self.elements = elements
    
    def insert_figure(self, fig):
        self.elements.append(fig)

class InlineText:
    def __init__(self, text, italic=False, type='normal'):
        self.text = text
        self.italic = italic
        self.type = type

    def __repr__(self):
        return self.text

class DisplayFigure:
    def __init__(self, figname=None, caption=None):
        self.figname = figname
        self.caption = caption
        self.type = 'figure'
