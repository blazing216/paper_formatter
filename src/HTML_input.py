import re
p_re = re.compile(r'<p>(.*?)</p>', re.DOTALL)
import core

class HTMLReader:
    def __init__(self, html=None):
        self.html = html

    def parse(self):
        paper = core.Paper()
        for para_str in self.get_paragraphs():
            elements = self.parse_paragraph(para_str)
            paper.insert_paragraph(elements)

        return paper

    def get_paragraphs(self):
        res = re.findall(p_re, self.html)
        return [e.replace('\n', '') for e in res]

    def parse_paragraph(self, string):
        elements = parse_string(string, 
            [parse_italic, parse_formula, parse_figure])
        return elements


def parse_italic(string):
    normal_texts = re.split(r'<em>.*?</em>',
        string)
    italic_texts = re.findall(r'<em>(.*?)</em>',
        string)
    assert len(normal_texts) == len(italic_texts) + 1, \
        'normal texts should be one more than italic texts'
    texts = []
    for text_normal, text_italic in zip(normal_texts, italic_texts):
        if len(text_normal) > 0:
            texts.append(core.InlineText(text_normal, italic=False))
        if len(text_italic) > 0:
            texts.append(core.InlineText(text_italic, italic=True, type='italic'))
    text_normal = normal_texts[-1]
    if len(text_normal) > 0:
        texts.append(core.InlineText(text_normal, italic=False))
    return texts

def parse_formula(string):
    normal_texts = re.split(r'\$.*?\$',
        string)
    formula_texts = re.findall(r'\$(.*?)\$',
        string)
    assert len(normal_texts) == len(formula_texts) + 1, \
        'normal texts should be one more than italic texts'
    texts = []
    for text_normal, text_formula in zip(normal_texts, formula_texts):
        if len(text_normal) > 0:
            texts.append(core.InlineText(text_normal))
        if len(text_formula) > 0:
            texts.append(core.InlineText(text_formula, type='formula'))
    text_normal = normal_texts[-1]
    if len(text_normal) > 0:
        texts.append(core.InlineText(text_normal))

    return texts

def parse_figure(string):
    normal_texts = re.split(r'<figure>.*?</figure>',
        string)
    figure_texts = re.findall(r'<figure>(.*?)</figure>',
        string)

    elements = []
    for text_normal, text_figure in zip(normal_texts, figure_texts):
        if len(text_normal) > 0:
            elements.append(core.InlineText(text_normal))

        if len(text_figure) > 0:
            res = re.findall(r'<img src="(.*?)"\s*\S*>', text_figure)
            if len(res) >= 0:
                figname = res[0]
            else:
                figname = None

            res = re.findall(r'<figcaption>(.*?)</figcaption>', text_figure)
            if len(res) >= 0:
                caption = res[0]
            else:
                caption = None
            elements.append(core.DisplayFigure(figname=figname,
                caption=caption))

    text_normal = normal_texts[-1]    
    if len(text_normal) > 0:
            elements.append(core.InlineText(text_normal))

    return elements

def parse_string(string, parse_funcs):
    elements = None
    for func in parse_funcs:
        if elements is None:
            elements = func(string)
        else:
            new_elements = []
            for e in elements:
                if e.type == 'normal':
                    new_elements += func(e.text)
                else:
                    new_elements.append(e)
            elements = new_elements.copy()
    return elements