from latex2mathml import converter
from lxml import etree

class Latex2Word:
    def __init__(self, mml2omml_stylesheet_path=None):
        if mml2omml_stylesheet_path is None:
            mml2omml_stylesheet_path = \
                '/Users/xuyihe/paper_formatter/src/MML2OMML.XSL'
        self.xsl = mml2omml_stylesheet_path
        xlst = etree.parse(self.xsl)
        self.transform = etree.XSLT(xlst)

    def to_mathml(self, latex):
        mathml = converter.convert(latex)
        return mathml
    
    def to_omml(self, latex):
        mathml = self.to_mathml(latex)
        dom = self.transform(etree.fromstring(mathml))
        return etree.tostring(dom.getroot())

    def insert_to_paragraph(self, latex, paragraph):
        mathml = self.to_mathml(latex)
        dom = self.transform(etree.fromstring(mathml))
        paragraph._element.append(dom.getroot())

