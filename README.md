# Paper formatter
Reformat a paper to publishable formats.

# Target
Support
- Input
  - HTML
- Output
  - Word
  - Latex
- Support plugins for other inputs (Markdown, Latex)
  - Do not reply the core logic on certain format

# Structure
- Core
  - A big dictionary?
- Input plugins
  - HTML
- Output plugins
  - Word
  - Latex
- Style plugins
  - GJI

# Milestones
- [x] 0.1.0 convert plain text
- [x] 0.2.0 convert text with italic
- [x] 0.3.0 convert text with in-line formula
- [x] 0.4.0 convert paragraphs
- [x] 0.5.0 create a tutorial
- [x] 0.6.0 convert paragraphs with display formulas
- [x] convert paragraphs with figures
- [x] 0.7.0 convert paragraphs with figures and captions
- [ ] update the tutorial
- [ ] convert sections
- [ ] convert title, author, affliations, abstract and keywords
- [ ] update the tutorial
- [ ] convert paragraphs with references to display formulas
- [ ] convert paragraphs with references to figures
- [ ] update the tutorial
- [ ] convert paragraphs with citations
- [ ] convert paragraphs with references to citations
- [ ] update the tutorial
- [ ] add the GJI style
- [ ] add the Latex output

